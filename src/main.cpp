#include "DHT.h"
#include <Homie.h>

// Custom settings
HomieSetting<long> sensorInterval("sensorInterval", "The number of seconds between sensor readings");

HomieNode temperatureNode("temperature", "Temperature", "temperature");
HomieNode humidityNode("humidity", "Humidity", "humidity");

#define DHTPIN 14 // D5 of NodeMCU is GPIO14
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

unsigned long lastSampleMillis = 0;

// Read temperature and humidity values from DHT sensor
bool readSensor() {
	float tempFahrenheit = dht.readTemperature(true);
	float humidity = dht.readHumidity();
	if (isnan(tempFahrenheit) || isnan(humidity)) {
		Serial.println(F("Failed to read from DHT sensor!"));
		return false;
	}
	Serial << tempFahrenheit << " °F, " << humidity << " H" << endl;
	temperatureNode.setProperty("degrees").send(String(tempFahrenheit));
	humidityNode.setProperty("percent").send(String(humidity));
	return true;
}

// Function that will be called when set up in normal mode
void homieSetup() {
	Serial << "⌛ Sensor interval: " << sensorInterval.get() << "s" << endl;
}

// Function that will be looped in normal mode
void homieLoop() {
	unsigned long loopStartTime = millis();
	if (loopStartTime - lastSampleMillis >= sensorInterval.get() * (unsigned)1000) {
		   Serial << loopStartTime - lastSampleMillis
		   << "ms elapsed since last sample taken, reading values"
		   << endl;
		readSensor();
		lastSampleMillis = millis();
	}
}

void setup() {
	Serial.begin(9600);
	Serial << endl << endl;

	dht.begin();

	Homie_setFirmware("temp-humidity", "1.0.0");
	Homie.setLoopFunction(homieLoop);

	sensorInterval
		.setDefaultValue(60)
		.setValidator([](long candidate) { return (candidate >= 0); });

	Homie.setSetupFunction(homieSetup);

	// Set node property and attributes
	temperatureNode.advertise("degrees")
		.setName("Degrees")
		.setDatatype("float")
		.setUnit("°F");
	humidityNode.advertise("percent")
		.setName("Percent")
		.setDatatype("float")
		.setUnit("%");

	Homie.setup();
}

void loop() {
	Homie.loop();
}
