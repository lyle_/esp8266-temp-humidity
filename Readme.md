# ESP8266 Temperature and Humidity Sensor

An ESP8266-based temperature and humidity sensor device, based on my
[ESP8266 starter](https://gitlab.com/lyle_/esp8266-platformio-homie) project.
Uses the [PlatformIO Core](https://docs.platformio.org/en/latest/core.html) build
toolchain and [homie-esp8266](https://github.com/homieiot/homie-esp8266/) for
handling WiFi and MQTT duties.

## Software requirements

* `make` for streamlining the build process
* [Platform IO Core (CLI)](https://docs.platformio.org/en/latest/installation.html)
* Install the ESP8266 framework for Homie: `platformio lib install 555`

## Hardware requirements

* ESP8266 development board (~$5)
* [DHT11 or DHT22](https://learn.adafruit.com/dht) temperature and humidity sensor (~$2)
* Power source (micro USB or batteries, left as an exercise for the reader)
* Enclosure and stripboard (optional, but I used [this](https://www.banggood.com/10pcs-75-x-54-x-27mm-DIY-Plastic-Project-Housing-Electronic-Junction-Case-Power-Supply-Box-p-1168741.html) and [this](https://www.banggood.com/Geekcreit-10pcs-50x70mm-FR-4-2_54mm-Double-Side-Prototype-PCB-Printed-Circuit-Board-p-1257154.html)) (~$2)

## Configuring the device

* Create a `data/homie` directory in the project which the PlatformIO toolchain
will turn into a SPIFFS filesystem: `mkdir -p data/homie`.
* Copy the sample configuration file into that directory: `cp config.example.json data/homie/config.json`.
* Change the `name`, `device_id`, `wifi.*`, and `mqtt.*` values to match your WiFi
network and MQTT broker (I recommend signing up for a free account at [shiftr.io](shiftr.io)).
* Flash the SPIFFS image: `make uploadfs`
The Makefile includes the common operations you'll need. Run `make help` for a list.

## Running

Plug in the device and follow along; if you run into any snags, the essence of
the procedure follows the [Homie for ESP8266 Quickstart](https://homieiot.github.io/homie-esp8266/docs/develop-v3/quickstart/getting-started/#1b-with-platformio).
Note that we use the development branch of the library since it supports Homie 3.0.1.

	# Compile sources (this will also pull in the needed dependencies)
	make build
	# Upload the firmware to the device
	make upload

Since the configuration has been flashed to the device's onboard filesystem,
if everything goes well it should simply connect to your WiFi access point
and your MQTT broker and begin publishing data.

To monitor serial output while the device is plugged into your dev machine, run

	make monitor

## Developing

In order to assist various linting utilities, the build process will generate a
[JSON Compilation Database](https://clang.llvm.org/docs/JSONCompilationDatabase.html)
if the [bear](https://github.com/rizsotto/Bear) utility is available.

You can also generate helpful configuration files which work with Vim linters by running:

	make vim-init
