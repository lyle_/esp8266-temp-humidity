" Project-specific (Neo)Vim settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
" Use clang_complete flags to tell clang where library paths are
let g:ale_cpp_clang_options = system("cat .clang_complete | tr '\n' ' '")
