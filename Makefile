.PHONY: all build clean upload uploadfs monitor init-vim help
all: build

# Use `bear` if available to generate a compilation database for clang tooling.
bear_path := $(shell which bear 2> /dev/null)

build: ## Compile the application
    ifdef bear_path
		bear platformio run
    else
		$(warning *** No 'bear' found in $$PATH, consider installing it to create a compilation database for linters)
		platformio run
    endif

clean: ## Clean up temporary files
	platformio run --target clean

upload: ## Upload firmware to a target
	platformio run --target upload

uploadfs: ## Upload SPIFFS filesystem to a target
	platformio run --target uploadfs

upload-monitor: upload monitor ## Upload firmware and monitor serial output

monitor: ## Monitor serial output from device
	platformio device monitor

init-vim: ## Create configuration files for linters used with Vim
	platformio init --ide vim

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@printf "\nRun 'pio --help' for PlatformIO-specific help\n"
